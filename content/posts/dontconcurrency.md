---
title: "Don't complicate concurrency"
date: 2023-07-29T19:57:47+02:00
draft: false 
---

# A Simple Task, Complicated Approach.

In the world of programming, concurrency can be a powerful tool for optimizing performance and handling multiple tasks efficiently. However, it's essential to use it judiciously and avoid overcomplicating things in the pursuit of concurrency.

Recently, at work, I encountered a fascinating challenge. I was tasked with refactoring a program that exhibited an overly complicated approach to concurrency. The initial implementation heavily relied on complex synchronization mechanisms and threads, making it arduous to reason about and maintain. The code was exceptionally convoluted, with processes interconnected via pipelines, processes spawning more processes, and threads intertwined within. The relationships between these processes were established through data transfers. **If I were to depict what hell looks like in code, this would be this situation!**

![Image](https://cdn.midjourney.com/c5e40462-eef5-4dc1-b4f8-7737c1cc0bd0/0_3.png)

## Many Challenges In The New Way.

This task showed me that not only using concurrency is demanding but also **maintaining the code in its simplest form**.

Now, let us cast a discerning eye upon two operations.

```python
for item in items:
    pipeline.put(item)
```

And second.


```python
while not pipeline.empty():
    item = pipeline.get()

    update(item)
```

One process is producing, and the second is consuming—simple, right?

So, how about multiple processes and multiple pipelines? It gets complicated when trying to realize relationships between processes and the destination of each process.

This is a simple example. When increasing the number of code instructions in separate processes, it also complicates the code readability. **Code readability is really important in project maintenance!**

### Many Processes, Many Paths to Transport Data.

Let's create multiple processes and send them some data.

```python
# Process's function
def calc(pipeline1, pipeline2):
    p1 = Process(target=update, args=(pipeline1,))   
    p2 = Process(target=update, args=(pipeline2,))  

    p1.start()
    p2.start()

    p1.join()
    p2.join()

# Create pipelines and process and start it

queue1 = multiprocessing.Queue()
queue2 = multiprocessing.Queue()

p1 = Process(target=calc, args=(queue1, queue2,))
p1.start()
p1.join()
```

A process creates another process and sets up pipelines; there are two channels through which data is moving. Observing and focusing on multiple pathways might be challenging for the human brain.

### My Little Worker!

How about creating threads in process?

```python
def second_calc():
    t1 = Thread(target=update, args=(25,))
    t1.start()
    
    ...

    t1.join()

def calc():
    p1 = Process(target=second_calc)    
    p1.start()

    ...

    p1.join()

```

Things are getting weird; nested concurrent instructions are being extended, complicating the situation and increasing the risk of catastrophe.

### Should I Finish My Job?

One technique to notify the process consumer that all data has been produced and the task is done is to set some flag.

```python
def product():
    ...
    # Example how to tell consumer the job is done
    queue.task_done()
    ...
```

I've seen some examples where programmers overprotect the consumer function to ensure it finishes its jobs by combining multiple primitive techniques.

```python
def consume():
    # More tools = more efficiency?
    while queue.empty() and finish_flag.set():
        ...
```

That's not the way! Using more can have the opposite effect and never end the loop.

### Another Process Terminates the Program.

Remember, that is a bad idea, to exit the program from one created process.

```python
def bad_function():
    for number in numbers(1, 25):
        process(number)

    sys.exit(-1)

p1 = Process(target=bad_function)
p1.start()

```

This could be a super irritating for others.

### Web Of Pipelines.

Pipelines serve as a conduit for inter-process communication, yet one of the key challenges lies in comprehending the intricate relationship between processes and their dependencies.

```python
# Definition of the producers and consumers functions

def produce_numbers(queue):
    for n in range(100):
        queue.put(n)

def produce_chars(queue):
    for c in "abcxdrsfbdsfssre":
        queue.put(c)

def consume_numbers(queue):
    while not queue.empty():
        number = queue.get()

def consume_chars(queue):
    while not queue.empty():
        char = queue.get()


# Create and run processes

queue1 = multiprocessing.Queue()
queue2 = multiprocessing.Queue()

producer1 = multiprocessing.Process(target=produce_numbers, args=(queue1,)) 
producer2 = multiprocessing.Process(target=produce_chars, args=(queue2,)) 

consumer1 = multiprocessing.Process(target=consume_numbers, args=(queue1,)) 
consumer2 = multiprocessing.Process(target=consume_chars, args=(queue2,)) 

producer1.start()
producer2.start()
consumer1.start()
consumer2.start()

```

We have two producers and two consumers; the case is not so complicated yet. But what about when we increase the number of producers and consumers? Debugging that case will be a challenge. There are different kinds of operations in each producer-consumer relation, which might be frustrating when trying to determine who is doing what.


## Power of Human Creativity, Limitations of the Machines.

At times, when endeavoring to spawn numerous processes and inundate the pipeline with copious data, it may lead to a system crash. If you happen to invoke your program via Jenkins, it could result in the program's abrupt termination due to exceeding the maximum limit.

Based on my experience, optimal performance is invariably achieved through reduction and precision in coding. Overloading often stems from the boundless power of human creativity. Our minds give birth to a myriad of ideas, and we attempt to incorporate them all into the code, yet such an endeavor frequently culminates in undesirable outcomes.

```python
# I'm fast!
def fast_calc():
    # Invoking really fast calculations
```

Is it always necessary to use concurrency? Probably not. There are plenty of techniques to accelerate calculations and optimize them. When it fails to produce the desired effect, then consider employing concurrency - but always be prosaic!

```python
# This is a good case to use concurrency!
def heavy_slow_calc():
    # Invoking slow calculations    
```


## Be prosaic.

The experience taught me a valuable lesson: while concurrency can offer great benefits, it's vital to strike a balance and avoid overengineering solutions. Complicated concurrent systems can lead to subtle bugs, difficult debugging, and increased development time. Simplicity should be the guiding principle, and concurrency should be employed thoughtfully where it genuinely adds value.

Remember, concurrency can be a double-edged sword. When used wisely, it can improve performance, responsiveness, and user experience. But when overused or misapplied, it can introduce unnecessary complexity and unintended consequences.

When contemplating the parallelization of instructions, pause and ask yourself: **can we accelerate this process without resorting to much concurrency?** Most likely, the answer is yes!

Here are some invaluable tips that can pave the way for achieving superior performance in our immensely resource-intensive function.

### Try to use a better algorithm.

Try to find and use a better algorithm to achieve what you want; some algorithms have better efficiency.

### Cache results to avoid wasting CPU's time by repeating operations.

It is good idea to use memory, getting item from memory is quite fast, repeat whole calculation not.

### Use database or any kind of storage.

This is an extension of caching. If it is possible to store calculation data in the database to skip some items or optimize certain operations, then do it. Response time is gold in today's modern technology world.

### In the case where you have I/O bound tasks, use async, do not waste time on waiting for the resources.

This is a light concurrency in my opinion. When we are blocked because we are waiting for a response from another process, it is a good idea to start other tasks that will also wait for resources. A good example is sending multiple requests to the server and waiting for all of them. The maximum time for that operation is the time of the request that waited the longest for a response.

### In the case where you have CPU-bound tasks, use multiprocessing.

When you have a really heavy operation, utilize multiprocessing, but keep it simple! Remember, you can use shared memory to share big data between processes. Use message passing techniques like Pipelines or Queues for small data.

# Finishing words

During my recent work experience, I encountered a challenging task that involved dealing with complex concurrency. The situation demanded a deep understanding of parallel execution and synchronization, which led to numerous complications. As I delved into solving the problem, I realized the importance of simplifying our approach and embracing a more straightforward path.

Concurrency can be a **powerful tool** when used correctly, but it's essential to strike a balance between complexity and simplicity. Although intricate solutions might seem impressive at first glance, they often come at the cost of maintainability and debugging. In contrast, choosing simplicity allows us to keep our code manageable and comprehensible.

By reducing complexity, we can improve the clarity of our code and make it easier to reason about. This, in turn, enhances collaboration among team members and facilitates the maintenance of the project in the long run.

While working on the intricate concurrent task, I faced several challenges that I wouldn't have encountered with a more straightforward design. Debugging became a time-consuming process, and the risk of introducing new bugs was always lurking. It made me appreciate the power of simplicity even more.

When dealing with concurrency, it's crucial to remember that we should strive to minimize the number of moving parts and dependencies. Simple solutions are often more robust and less prone to unexpected issues.

Remember, when it comes to concurrency, **be prosaic**.







