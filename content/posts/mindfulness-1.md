---
title: "Czy na pewno wiesz, na czym polega Mindfulness?"
date: 2023-08-16T19:57:47+02:00
draft: false
---

# Chaotyczny świat i jego smutki

Nowoczesny świat staje się coraz bardziej skomplikowany, co utrudnia poruszanie się w nim oraz odnalezienie się przeciętnemu człowiekowi. Ponadto panuje plaga samotności, co nie jest dobrą wiadomością. Jak wiadomo, człowiek jest istotą społeczną i jest tak zaprogramowany, że gdy popada w stan, gdzie brak jest innych ludzi, jego ciało natychmiast domaga się kontaktu z innym człowiekiem, manifestując to wszelkimi sygnałami w postaci negatywnych emocji. Na świecie jest wysyp chorób psychicznych, trudno znaleźć osobę, która nie ma rysy na swojej psychice.

Pewien człowiek wpadł na cudowny pomysł: przestać myśleć i skupić się na tym, co jest tu i teraz.

![book](https://i.postimg.cc/jSYLFhmx/IMG-4747.jpg)

# Ucieczka ze świata marzeń

Jon Kabat-Zinn, twórca popularnego dziś programu MBSR (redukcja stresu dzięki medytacji mindfulness), wpadł na pomysł, by **trenować mózg**, aby był bardziej uważny w chwilach obecnych i mniej podążał za myślami. Koncepcja Kabat-Zinna jest bardzo prosta: jak najdalej od myśli i jak najbliżej aktualnej chwili, by być tu i teraz oraz doświadczać wszystkiego, co obfite w chwili obecnej. Pomysł Jona brzmi bardzo prozaicznie, prawda?

Kabat-Zinn w swojej klinice redukcji stresu wypróbował program na m.in. weteranach wojennych. Rezultaty były zaskakujące: weterani nabrali dużej odporności na stres, a ich tiki oraz traumy, które posiadali, zmalały lub znikneły! Jak to możliwe? Okazuje się, że to, co myślimy, ma bardzo duży wpływ na nas, nasze nastawienie oraz sposób przyjmowania rzeczy, które nam się przytrafiają. To konkretny schemat związków chemicznych w mózgu! Jednym zdaniem, to, jak myślimy, ma wpływ na naszą chemię w mózgu.

## Twoje najpotężniejsze narzędzie

Mózg jest potężnym narzędziem, a co najbardziej fascynujące, to to, że może się sam modyfikować. Mowa o **neuroplastyczności**, czyli **zdolności do zmiany połączeń synaptycznych w mózgu**. Dzięki systematycznemu treningowi można wpłynąć na mózg. MBSR to trening uważności, skupiania się na chwili obecnej oraz powrót ze świata marzeń. Jeśli nasz mózg złapał nas w pułapkę swojej myśli i wciągnął nas w nasz świat iluzji, to wymuszanie na mózgu bycia w chwili obecnej ma pozytywne konsekwencje, m.in.:
- Większa ilość szarej substancji w mózgu
- Lepsza pamięć
- Szybsze przetwarzanie informacji
- Mniejszy stres, między innymi związany z samotnością
- Osłabione połączenia z amygdalą, czyli ośrodkiem strachu
- Mniejsze ciśnienie krwi
- Zredukowane objawy alergiczne lub ich zanik

I wiele, wiele więcej.

**Psychika ma ogromny wpływ na nasz dobrostan**. Ma ona także kuriozalny wpływ na ciało. Wyćwiczona uważność sprawia, że pozostałe "moduły" w ciele są sprawniejsze oraz zdrowsze. Jest to niesamowite, bo byliśmy wychowywani w przekonaniu, że na wszystko trzeba brać tabletki, a one mają swoje skutki uboczne. Coraz więcej badań naukowych pokazuje, że naprawiając stan psychiczny, naprawiamy również ciało. Nie potrzebna jest tabletka. Potrzebna jest chwila czasu oraz siła woli.

## Trochę czasu i wytrwałości

W klinice redukcji stresu proces "leczenia" przy użyciu mindfulness trwał 8 tygodni, gdzie dziennie pacjenci musieli poświęcać około 45 minut na medytację. Nie było to też wymuszane. Jeśli ktoś nie był w stanie wytrwać 45 minut, mógł wytrwać tyle, ile mógł. Głównym czynnikiem była determinacja i chęci pacjenta.

## Siła nie działania

Jon Kabat-Zinn zwracał uwagę na nie działanie. Nasza ludzka natura za wszelką cenę chce widzieć efekty. Chce, aby wyniki były natychmiastowe. To jest sprzeczne z rozwojem, który wymaga czasu, by nabyć nowe umiejętności. Nie działanie oznacza, że wszystko toczy się swoimi prawami. **Panuje porządek i rzeczy kierują się zgodnie ze swoim biegiem**. W Mindfulness uczy się, by delektować się nie działaniem, nie oceniać, tylko tkwić w chwili, płynąć zgodnie z nurtem.

## Nie kurs, a ścieżka

Z Mindfulness uczyniono tragiczną rzecz! Zrobiono z tego kurs, po którym mamy otrzymać wyleczenie. To nieprawda! Większość trenerów sprzedaje swoje kursy, wyliczając listę cudownych efektów, które mamy mieć po uzyskaniu certyfikatu. MBSR stał się maszynką do zarabiania pieniędzy.

W kursach uczą paru ćwiczeń, które głównie polegają na oddychaniu oraz skanowaniu ciała. To tylko część tego, co znajduje się w MBSR. Program Kabat-Zinna to przede wszystkim ścieżka życiowa, na którą wkraczasz. To zmiana nastawienia, nawyków, to ćwiczenie oddechu, to docenianie natury, to nieocenianie, to trening bycia, gdziekolwiek jesteś.

Sam **Kabat-Zinn nie jest zadowolony** z tego, co ludzie zaczęli robić z MBSR. Jego program wywodzi się m.in. z medytacji buddyjskiej zazen. Tam są nauki mnichów odnośnie życia i świadomości, które są pomijane w tłumaczeniu zasad i ćwiczeń mindfulness. To duży błąd, ponieważ ta wiedza lepiej pozwala zrozumieć koncepcje oraz pozostaje w pamięci na dłużej.

## Przewodnik

Mindfulness to przede wszystkim **przewodnik uważnego życia**. To cały zestaw praw, myśli i porad, jak żyć uważnie oraz jak być wolnym od stresu. To techniki postawy medytacji, to życiowe rady, jak sobie radzić z myślami, to docenianie natury. Czy wiedzieliście, że w mindfulness są techniki medytacji gór bądź jeziora? Mindfulness to także pytania natury życiowej, które potem towarzyszą do końca życia.

Gdybym miał porównać życie do oceanu, a ludzi do kapitanów statków, to **mindfulness w ich rękach byłoby mapą**, jak po tym życiu płynąć, by nie zatonąć.

To także poradnik, jak należy przetwarzać emocje, czym one są, po co są i dlaczego są.

Mindfulness to także świadome mycie zębów, świadomy spacer oraz bycie gdziekolwiek się znajdujesz.

## Trzymaj się prawdy

Jeśli masz pragnienie żyć kiedykolwiek w pełni świadomie, nie uciekać do świata marzeń i fantazji, przestać się bać oraz cieszyć się chwilą obecną, to nie kupuj najpierw kursu mindfulness! Sięgnij do źródeł, skąd mindfulness pochodzi. Kup [książki](https://www.google.pl/search?q=kabat+zinn+ksi%C4%85%C5%BCki&sca_esv=557502889&sxsrf=AB5stBgI1EqTPyD_d3oiRrFnlmGrik-rPQ%3A1692211058459&source=hp&ei=chfdZL20GI2Vxc8PlOOHoAw&iflsig=AD69kcEAAAAAZN0lgtD18UbCg8xS-vILlyrWsW6KfYYt&oq=kabat-zin+ksi%C4%85%C5%BC&gs_lp=Egdnd3Mtd2l6IhFrYWJhdC16aW4ga3NpxIXFvCoCCAAyBhAAGBYYHkiHEVAAWNMMcAB4AJABAJgBzwGgAfQOqgEGMC4xNC4xuAEDyAEA-AEBwgILEAAYgAQYsQMYgwHCAgUQABiABMICCxAuGIoFGLEDGIMBwgIHECMYigUYJ8ICCxAAGIoFGLEDGIMBwgILEC4YgAQYsQMYgwHCAggQABiABBixA8ICCBAuGIAEGLEDwgIIEC4YsQMYgATCAgUQLhiABMICCxAuGIAEGMcBGK8BwgIIEC4YgAQYywHCAggQABiABBjLAcICBxAAGA0YgATCAgYQABgeGA3CAgkQABgeGPEEGArCAggQABgWGB4YCg&sclient=gws-wiz) Kabat-Zinna i przekonaj się, czym mindfulness jest. Całą ścieżkę do świadomości możesz przejść samodzielnie, ponieważ to tylko mapa pokazująca, w którą stronę musisz podążać. Dzięki książce zaoszczędzisz czas i pieniądze.

Wejdź na ścieżke uważności i obseruj jak Twoje ciało się leczy.
