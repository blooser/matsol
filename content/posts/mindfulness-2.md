---
draft: false
date: 2023-09-05T09:24:36+02:00
title: "Uważność jako technika przełamywania automatycznych reakcji"
---

# Czy To Naprawdę Ja?

Cześć! Wydaje mi się, że może nie zdajesz sobie sprawy, ale większość naszego życia toczy się na autopilocie. Wykonujemy codzienne czynności automatycznie, często nie zdając sobie z tego sprawy. Posiadamy wytrenowane wzorce i schematy, którymi kieruje nasz mózg. Czy kiedykolwiek zdarzyło Ci się myć okna, zmywać naczynia lub szczotkować zęby, a potem nie być w stanie dokładnie przypomnieć sobie, co robiłeś tuż po tych czynnościach? Prawdopodobnie tak!

Jednak okazuje się, że jesteśmy tak zaabsorbowani myślami, że przegapiliśmy wiele pięknych momentów i bogactwa otaczającego nas świata. Często nasze fantazje w umyśle przesłaniają to, co dzieje się wokół nas. Czy jesteś gotów na chwilę oderwać się od rutyny i skupić na tym, co dzieje się wokół ciebie? To może być bolesne doświadczenie, prawda? Rozwijanie uważności to proces wymagający wysiłku, a co zabawne, uważność to także stan relaksu, stan bezczynności, choć mimo to może być trudna.

![img](https://cdn.midjourney.com/b41bec35-be12-43eb-8fc5-4ad47c0c0bc8/0_2.png)

## Schematy i Automatyczne Reakcje

Nasz prymitywny mózg działa według dwóch głównych zasad: "walcz lub uciekaj". Z biologicznego punktu widzenia jest to przydatny schemat przetrwania, ale w dzisiejszym nowoczesnym i przeważnie bezpiecznym świecie może prowadzić do wielu problemów. Nieodpowiednie reakcje na stres, ataki lęku, przegapione szczegóły czy nieodpowiednie reakcje organizmu to tylko niektóre z problemów wynikających z automatycznych reakcji, którymi kieruje się nasz organizm. Nasz prymitywny mózg nie jest dostosowany do współczesnego świata i często interpretuje wiele rzeczy jako zagrożenie, co prowadzi do aktywacji reakcji i zachowań nieadekwatnych do sytuacji. Czy zdajesz sobie sprawę, że tak samo, jak mózg może być wytrenowany do określonych reakcji, można go również "przekwalifikować", aby działał zgodnie z naszymi intencjami?

## Uważność jako Ścieżka Wyboru

Proces skupiania uwagi jest kluczem do nauczenia mózgu, jak działać w konkretnej sytuacji. Niestety jest to trudne i wymaga czasu, ponieważ mózg będzie się bronić przed zmianami, które niekoniecznie mu odpowiadają.

Podczas sytuacji, w której odczuwasz lęk lub frustrację, spróbuj skupić całą swoją energię na zatrzymaniu się i zadać sobie pytania: "Jak się teraz czuję?", "Dlaczego tak się czuję?", "Czy jestem teraz świadomy?" Następnie rozważ sytuację z szerszej perspektywy, błądź myślami i próbuj zrozumieć źródło, które wywołuje tę reakcję w twoim ciele. Następnie, po wyjściu ze schematu, podejmij inną decyzję, poprzedzoną pytaniem: "Co mogłem zrobić inaczej do tej pory?" i działaj.

## Zmiana i Neuroplastyczność

Przemiana mózgu, czyli neuroplastyczność, jest wynikiem tworzenia nowych połączeń synaptycznych i konstruowania nowych reakcji na bodźce. Jednym z kluczowych bodźców dla mózgu jest nowość, dlatego odczuwamy satysfakcję z podróży czy odkrywania nowych rzeczy! Ten mechanizm można wykorzystać do przebudowywania naszego umysłu.

Reakcje stanowią pole, w którym możesz się poruszać i wyznaczać kierunek.

**Niech uważność będzie z Tobą!**
